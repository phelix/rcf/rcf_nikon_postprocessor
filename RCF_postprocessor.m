function varargout = RCF_postprocessor(varargin)
% RCF_PREPROCESSOR MATLAB code for RCF_preprocessor.fig
% Tool for cutting and modifying RCF image data, taken with the NIKON
% Coolscan 9000 ED.
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Copyright (C) 2021 by it's contributors.
% Some rights reserved. See COPYING, CREDITS.
%
% This file is part of RCF Nikon postprocessor.
%
% RCF Nikon postprocessor is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% long with this program.  If not, see <https://www.gnu.org/licenses/>.

% Last Modified by GUIDE v2.5 08-Sep-2013 15:34:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RCF_postprocessor_OpeningFcn, ...
                   'gui_OutputFcn',  @RCF_postprocessor_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before RCF_preprocessor is made visible.
function RCF_postprocessor_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to RCF_preprocessor (see VARARGIN)

% Choose default command line output for RCF_preprocessor
handles.output = hObject;

% first start, clean up everything
try rmappdata(0, 'data'); end

% and init
%process status:
%0 => after init, single value
%1 => after loading all RCFs into array; vector with val for each layer
%2 => after rotation
%3 => after cut; now we have a new, smaler ImageArray
data.parameters.rcfpath = [];
data.parameters.rcffiles = [];

setappdata(0,'data', data);

handles.grid=0;
handles.rotangle=0;
handles.sliderval=0;
handles.region=3;
handles.process = 0;
handles.panel = 2; %default search bright and black scratches
set(handles.brightbutton,'Value',0);
set(handles.darkbutton,'Value',0);
set(handles.bothbutton,'Value',1);

guidata(hObject, handles);

% UIWAIT makes RCF_preprocessor wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = RCF_postprocessor_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

%give warning and introduction abount missing mex-file
if exist('findscratchesc','file')==0
    cprintf([1 0 0],'ERROR: findscratchesc mex-file not found, you have to compile first before running program ! \n \n type: mex findscratchesc.c \n \n if you running mex the first time \n type: mex -setup and choose a c-compiler \n after this run: mex findscratchesc.c \n\n');
    cprintf([0,0,0],'')
    close RCF_postprocessor
else
    new = dir('findscratchesc.c');
    old = dir('findscratchesc.m*');
    
    %if findscratches.c is newer mex-file => there was an update, please recompile
    %elo.c
    if new.datenum>=old.datenum
      cprintf([1 0 0],'ERROR: findscratchesc mex-file is older than findscratchesc.c => there was an update, please recompile findscratchesc.c \n \n type: mex findscratchesc.c \n \n');
      cprintf([0,0,0],'')
      close RCF_postprocessor
    end
end


% --- Executes on button press in loadrcf.
function loadrcf_Callback(hObject, eventdata, handles)
% hObject    handle to loadrcf (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = getappdata(0, 'data');
handles.process = 0;

if ~isempty(data.parameters.rcfpath)
    [data.parameters.rcffiles, data.parameters.rcfpath] = uigetfile(...
    {'*.tif','TIF-files (*.tif)'; '*.jpg','JPG-files (*.jpg)'; ...
    '*.*',  'All Files (*.*)'}, 'Pick a file', ...
    'MultiSelect', 'on', data.parameters.rcfpath);
else
    [data.parameters.rcffiles, data.parameters.rcfpath] = uigetfile(...
    {'*.tif','TIF-files (*.tif)'; '*.jpg','JPG-files (*.jpg)'; ...
    '*.*',  'All Files (*.*)'}, 'Pick a file', ...
    'MultiSelect', 'on');
end

if ~isequal(data.parameters.rcffiles,0)
    if iscell(data.parameters.rcffiles)        
        data.parameters.rcffiles = sort(data.parameters.rcffiles);
    else
        tmp=data.parameters.rcffiles;
        data.parameters.rcffiles={tmp};               
    end
    
    data.data.fileinfo = imfinfo(fullfile(data.parameters.rcfpath, data.parameters.rcffiles{1}));
    width=data.data.fileinfo.Width;
    height=data.data.fileinfo.Height;

    %create 4D image array, RGB+irchannel
    data.data.ImageArray = zeros(height, width, 4, length(data.parameters.rcffiles), 'uint16');  % 16bit/channel and files

    %fill the image array with content
    for frame = 1:length(data.parameters.rcffiles)
      file_name = fullfile(data.parameters.rcfpath, data.parameters.rcffiles{frame});
      filepoint = Tiff(file_name,'r');
      %RGB part of image
      filepoint.setDirectory(1);
      data.data.ImageArray(:,:,1:3,frame) = filepoint.read();
      
      if size(data.data.fileinfo,1) ~= 1
          % more channels if IR part is present
          %IR part of image
          filepoint.setDirectory(2);
          data.data.ImageArray(:,:,4,frame) = filepoint.read();
      else
          % old scanner, fake IR part ... CB
          data.data.ImageArray(:,:,4,frame) = rgb2gray(data.data.ImageArray(:,:,1:3,frame));
      end
      filepoint.close();
    end
    
    try data.data=rmfield(data.data,'ImageArray_cut'); end
    
    %every film loaded into array
    data.process = ones(length(data.parameters.rcffiles),1);
    setappdata(0, 'data', data);
    set(handles.rcfloaded, 'String', data.parameters.rcffiles);
    
    handles.width=width;
    handles.height=height;
    handles.xcenter=floor(handles.width/2);
    handles.ycenter=floor(handles.height/2);
    handles.dxmask=floor(handles.width*0.75);
    handles.dymask=floor(handles.height*0.75);
         
    set(handles.xcentermask,'String',num2str(handles.xcenter));
    set(handles.ycentermask,'String',num2str(handles.ycenter));
    set(handles.xmask,'String',num2str(handles.dxmask));
    set(handles.ymask,'String',num2str(handles.dymask));
    
    handles.currentRCFframe=1;
    set(handles.rcfloaded,'Value',handles.currentRCFframe);
    set(handles.undobutton,'Enable','Off');
        
    set(handles.saveall ,'Enable','On');
    
    guidata(hObject, handles);
    
    set(handles.statustxt,'String', 'All RCFs succesfully loaded...','ForegroundColor','blue');
%     pause(1.0);
    loadcurr(hObject, eventdata, handles);
end


% --- Executes on selection change in rcfloaded.
function rcfloaded_Callback(hObject, eventdata, handles)
% hObject    handle to rcfloaded (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns rcfloaded contents as cell array
%        contents{get(hObject,'Value')} returns selected item from rcfloaded
handles.currentRCFname=get(hObject,'String');
handles.currentRCFframe=get(hObject,'Value');
set(handles.undobutton,'Enable','Off');
guidata(hObject,handles);

loadcurr(hObject, eventdata, handles);



% --- Executes during object creation, after setting all properties.
function rcfloaded_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rcfloaded (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in savecurr.
function savecurr_Callback(hObject, eventdata, handles)
% hObject    handle to savecurr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function rcfloaded_KeyPressFcn(hObject, eventdata, handles)
% --- Executes on key press with focus on rcfloaded and none of its controls.
% deletes from rcffiles, process and ImageArray
if strcmp(eventdata.Key,'delete')
    data = getappdata(0, 'data');
    SV = get(hObject,{'string','value'});
    if ~cellfun('isempty',SV{1})
        SV{1}(SV{2}) = [];
        data.parameters.rcffiles(SV{2}) = [];
        data.process(SV{2}) = [];
        try
            data.data.ImageArray(:,:,:,SV{2}) = [];
            data.data.ImageArray_cut(:,:,:,SV{2}) = [];
        end
        set(hObject,'string',SV{1},'value',1);
        handles.currentRCFname = get(hObject,'String');
        handles.currentRCFframe = get(hObject,'Value');
        set(handles.undobutton, 'Enable', 'Off');
    end
    setappdata(0,'data',data);
    guidata(hObject,handles);
    drawnow % refresh GUI while in callback
    loadcurr(hObject, eventdata, handles);
end


function xmask_Callback(hObject, eventdata, handles)
% hObject    handle to xmask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xmask as text
%        str2double(get(hObject,'String')) returns contents of xmask as a double
data=getappdata(0,'data');
stat=0;

if ~isnan(str2double(get(hObject,'String')));
    handles.dxmask=round(str2double(get(hObject,'String')));
    if size(data.data.ImageArray,2)<=handles.dxmask
        handles.dxmask=size(data.data.ImageArray,2);
        set(handles.xmask,'String',num2str(handles.dxmask));
        stat=1;

    elseif handles.dxmask>0
        set(handles.xmask,'String',num2str(handles.dxmask));
    else
        handles.dxmask=1;
        set(handles.xmask,'String',num2str(handles.dxmask));
        stat=2;
    end
else
    set(handles.xmask,'String',num2str(handles.dxmask));
    stat=3;
end

guidata(hObject,handles);

try
    if ishandle(handles.rectangle)
        maskit_Callback(hObject, eventdata, handles);
    end
end

if stat==1
    statusmsg(handles, 'error: width must be smaller/equal image size !!!','red');
elseif stat==2
    statusmsg(handles, 'error: width must be at least 1 px !!!','red');
elseif stat==3;
    statusmsg(handles, 'error: width must be a number !!!','red');
end


% --- Executes during object creation, after setting all properties.
function xmask_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xmask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ymask_Callback(hObject, eventdata, handles)
% hObject    handle to ymask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ymask as text
%        str2double(get(hObject,'String')) returns contents of ymask as a double
data=getappdata(0,'data');
stat=0;

if ~isnan(str2double(get(hObject,'String')));
    handles.dymask=round(str2double(get(hObject,'String')));
    if size(data.data.ImageArray,1)<=handles.dymask
        handles.dymask=size(data.data.ImageArray,1);
        set(handles.ymask,'String',num2str(handles.dymask));
        stat=1;

    elseif handles.dymask>0
        set(handles.ymask,'String',num2str(handles.dymask));
    else
        handles.dymask=1;
        set(handles.ymask,'String',num2str(handles.dymask));
        stat=2;
    end
else
    set(handles.ymask,'String',num2str(handles.dymask));
    stat=3;
end

guidata(hObject,handles);

try
    if ishandle(handles.rectangle)
        maskit_Callback(hObject, eventdata, handles);
    end
end

if stat==1
    statusmsg(handles, 'error: height must be smaller/equal image size !!!','red');
elseif stat==2
    statusmsg(handles, 'error: height must be at least 1 px !!!','red');
elseif stat==3;
    statusmsg(handles, 'error: height must be a number !!!','red');
end


% --- Executes during object creation, after setting all properties.
function ymask_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ymask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in maskit.
function maskit_Callback(hObject, eventdata, handles)
% hObject    handle to maskit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%handles.data.ImageArray2=handles.data.Undo;
data=getappdata(0,'data');

fixx=get(handles.fixx_box,'Value');
fixy=get(handles.fixy_box,'Value');

% xcenter=handles.xcenter
xcenter = str2double(get(handles.xcentermask,'String'));
dx=handles.dxmask;
% ycenter=handles.ycenter
ycenter = str2double(get(handles.ycentermask,'String'));
dy=handles.dymask;

plotrcf(data.data.ImageArray,handles);
hold on
handles.rectangle=rectangle('Position',[xcenter-dx/2 ycenter-dy/2 dx dy], 'LineWidth',2, 'EdgeColor','b');
hold off
set(gcf,'WindowButtonUpFcn',{@update_fields,handles});

%workaround for bringing handles and hObject to update_fieldsint (is called
%by draggable-function with feval => can be done smoother...
setappdata(0,'handles_old',handles);
setappdata(0,'hObject_old',hObject);

if (fixx && ~fixy)
    draggable(handles.rectangle,'v',[0, handles.height], @update_fieldsint);
elseif (fixy && ~fixx)
    draggable(handles.rectangle,'h',[0, handles.width], @update_fieldsint);
elseif (fixx && fixy)
    draggable(handles.rectangle,'off', @update_fieldsint);
else
    draggable(handles.rectangle,'n',[0, handles.width, 0, handles.height], @update_fieldsint);
end

set(handles.cutit,'Enable', 'On');
guidata(hObject,handles);


% --- Executes on button press in cutit.
function cutit_Callback(hObject, eventdata, handles)
% hObject    handle to cutit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = getappdata(0, 'data');
handles.currentRCFframe=get(handles.rcfloaded,'Value');

data.Undo.process=data.process(handles.currentRCFframe); %process flag before rotation
data.Undo.handles=handles.process;
if data.Undo.process==3
    data.Undo.data=data.data.ImageArray_cut;
else
    data.Undo.data=data.data.ImageArray;
end

set(handles.xmask,'Enable','Off');
set(handles.ymask,'Enable','Off');

xcenter = int64(handles.xcenter);
dx=int64(handles.dxmask);
ycenter = int64(handles.ycenter);
dy=int64(handles.dymask);
if ~isfield(data.data, 'ImageArray_cut')
     data.data.ImageArray_cut = zeros(dy, dx, 4, length(data.parameters.rcffiles), 'uint16');  % 16bit/channel and files
elseif size(data.data.ImageArray_cut,1) ~= dy || size(data.data.ImageArray_cut,2) ~= dx
     data.data.ImageArray_cut = zeros(dy, dx, 4, length(data.parameters.rcffiles), 'uint16');  % 16bit/channel and files    
end

dylow=ycenter-floor(dy/2);
dyup=dylow+dy-1;
dxlow=xcenter-floor(dx/2);
dxup=dxlow+dx-1;
data.data.ImageArray_cut(:,:,1:3,handles.currentRCFframe)=data.data.ImageArray(dylow:dyup,dxlow:dxup,1:3,handles.currentRCFframe);
data.data.ImageArray_cut(:,:,4,handles.currentRCFframe)=data.data.ImageArray(dylow:dyup,dxlow:dxup,4,handles.currentRCFframe);

plotrcf(data.data.ImageArray_cut,handles);

[handles.height, handles.width, ~, ~]=size(data.data.ImageArray_cut);

set(handles.cutit,'Enable','Off');
set(handles.maskit,'Enable','Off');
set(handles.ycentermask,'Enable','Off');
set(handles.xcentermask,'Enable','Off');

set(handles.fixx_box,'Value',0);
set(handles.fixy_box,'Value',0);
set(handles.fixx_box,'Enable','Off');
set(handles.fixy_box,'Enable','Off');

set(handles.rotateit,'Enable','Off');
set(handles.rotate,'Enable','Off');

set(handles.irthreslider,'Enable','On');
set(handles.irthresval,'Enable','On');
set(handles.showir,'Enable','On');
set(handles.removedust,'Enable','On');
set(handles.regionedit,'Enable','On');
set(handles.undobutton,'Enable','On');
set(handles.brightbutton,'Enable','On');
set(handles.darkbutton,'Enable','On');
set(handles.bothbutton,'Enable','On');
set(handles.corners_button,'Enable','Off');

data.process(handles.currentRCFframe)=3;
handles.process=3;
setappdata(0,'data',data);

guidata(hObject,handles);

statusmsg(handles,[char(data.parameters.rcffiles(handles.currentRCFframe)), ' succesfully cutted...'],'blue');


function xcentermask_Callback(hObject, eventdata, handles)
% hObject    handle to xcentermask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of xcentermask as text
%        str2double(get(hObject,'String')) returns contents of xcentermask as a double
data=getappdata(0,'data');
stat=0;

if ~isnan(str2double(get(hObject,'String')));
    handles.xcenter=round(str2double(get(hObject,'String')));
    if size(data.data.ImageArray,2)<=handles.xcenter
        handles.xcenter=size(data.data.ImageArray,2);
        set(handles.xcentermask,'String',num2str(handles.xcenter));
        stat=1; 

    elseif handles.xcenter>=0
        set(handles.xcentermask,'String',num2str(handles.xcenter));
    else
        handles.xcenter=0;
        set(handles.xcentermask,'String',num2str(handles.xcenter));
        stat=2;   
    end
else
    set(handles.xcentermask,'String',num2str(handles.xcenter));
    stat=3;    
end

guidata(hObject,handles);

try
    if ishandle(handles.rectangle)
        maskit_Callback(hObject, eventdata, handles);
    end
end

if stat==1
    statusmsg(handles, 'error: xcenter must be smaller/equal image size !!!','red');
elseif stat==2
    statusmsg(handles, 'error: xcenter must be at least 0 px !!!','red');
elseif stat==3;
    statusmsg(handles, 'error: xcenter must be a number !!!','red');
end


% --- Executes during object creation, after setting all properties.
function xcentermask_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xcentermask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ycentermask_Callback(hObject, eventdata, handles)
% hObject    handle to ycentermask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ycentermask as text
%        str2double(get(hObject,'String')) returns contents of ycentermask as a double
data=getappdata(0,'data');
stat=0;

if ~isnan(str2double(get(hObject,'String')));
    handles.ycenter=round(str2double(get(hObject,'String')));
    if size(data.data.ImageArray,1)<=handles.ycenter
        handles.ycenter=size(data.data.ImageArray,1);
        set(handles.ycentermask,'String',num2str(handles.ycenter));
        stat=1;
    elseif handles.ycenter>=0
        set(handles.ycentermask,'String',num2str(handles.ycenter));
    else
        handles.ycenter=0;
        set(handles.ycentermask,'String',num2str(handles.ycenter));
        stat=2;
    end
else
    set(handles.ycentermask,'String',num2str(handles.ycenter));
    stat=3;
end

guidata(hObject,handles);

try
    if ishandle(handles.rectangle)
        maskit_Callback(hObject, eventdata, handles);
    end
end

if stat==1
    statusmsg(handles, 'error: ycenter must be smaller/equal image size !!!','red');
elseif stat==2
    statusmsg(handles, 'error: ycenter must be at least 0 px !!!','red');
elseif stat==3;
    statusmsg(handles, 'error: ycenter must be a number !!!','red');
end


% --- Executes during object creation, after setting all properties.
function ycentermask_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ycentermask (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function irthreslider_Callback(hObject, eventdata, handles)
% hObject    handle to irthreslider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
data=getappdata(0,'data');
handles.currentRCFframe=get(handles.rcfloaded,'Value');
set(handles.undobutton,'Enable','Off');

handles.sliderval=get(hObject,'Value');
set(handles.irthresval,'String', num2str(handles.sliderval));

if data.process(handles.currentRCFframe)>=3
    handles.data.bwIR=findscratches(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles);
    plotrcfdata(imoverlay(data.data.ImageArray_cut(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
else
    handles.data.bwIR=findscratches(data.data.ImageArray(:,:,4,handles.currentRCFframe),handles);
    plotrcfdata(imoverlay(data.data.ImageArray(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
end

guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function irthreslider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to irthreslider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function irthresval_Callback(hObject, eventdata, handles)
% hObject    handle to irthresval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of irthresval as text
%        str2double(get(hObject,'String')) returns contents of irthresval as a double
data=getappdata(0,'data');
handles.currentRCFframe=get(handles.rcfloaded,'Value');
set(handles.undobutton,'Enable','Off');

if ~isnan(str2double(get(hObject,'String')));
    handles.sliderval=str2double(get(hObject,'String'));
    if 1<handles.sliderval
        handles.sliderval=1;
        set(handles.irthresval,'String',num2str(handles.sliderval));
        set(handles.irthreslider,'Value', handles.sliderval);
        if data.process(handles.currentRCFframe)>=3
            handles.data.bwIR=findscratches(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles);
            plotrcfdata(imoverlay(data.data.ImageArray_cut(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
        else
            handles.data.bwIR=findscratches(data.data.ImageArray(:,:,4,handles.currentRCFframe),handles);
            plotrcfdata(imoverlay(data.data.ImageArray(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
        end
        guidata(hObject,handles);
        statusmsg(handles, 'error: threshold must be smaller/equal 1 !!!','red');    

    elseif handles.sliderval>=0
        set(handles.irthresval,'String',num2str(handles.sliderval));
        set(handles.irthreslider,'Value', handles.sliderval);
        if data.process(handles.currentRCFframe)>=3
            handles.data.bwIR=findscratches(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles);
            plotrcfdata(imoverlay(data.data.ImageArray_cut(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
        else
            handles.data.bwIR=findscratches(data.data.ImageArray(:,:,4,handles.currentRCFframe),handles);
            plotrcfdata(imoverlay(data.data.ImageArray(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
        end
        guidata(hObject,handles);
    else
        handles.sliderval=0;
        set(handles.irthresval,'String',num2str(handles.sliderval));
        set(handles.irthreslider,'Value', handles.sliderval);
        if data.process(handles.currentRCFframe)>=3
            handles.data.bwIR=findscratches(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles);
            plotrcfdata(imoverlay(data.data.ImageArray_cut(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
        else
            handles.data.bwIR=findscratches(data.data.ImageArray(:,:,4,handles.currentRCFframe),handles);
            plotrcfdata(imoverlay(data.data.ImageArray(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
        end
        guidata(hObject,handles);
        statusmsg(handles, 'error: threshold must be at least 0 !!!','red');
    end
else
    set(handles.irthresval,'String',num2str(handles.sliderval));
    set(handles.irthreslider,'Value', handles.sliderval);
    guidata(hObject,handles);
    statusmsg(handles, 'error: threshold must be a number !!!','red');
end


% --- Executes during object creation, after setting all properties.
function irthresval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to irthresval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in showir.
function showir_Callback(hObject, eventdata, handles)
% hObject    handle to showir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getappdata(0,'data');
handles.currentRCFframe=get(handles.rcfloaded,'Value');
figure

if data.process(handles.currentRCFframe)>=3
    handles.data.bwIR=findscratches(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles);
    imagesc(imoverlay(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe), handles.data.bwIR, [1 0 0]));
    colormap(gray)
    axis equal
else
    handles.data.bwIR=findscratches(data.data.ImageArray(:,:,4,handles.currentRCFframe),handles);
    imagesc(imoverlay(data.data.ImageArray(:,:,4,handles.currentRCFframe), handles.data.bwIR, [1 0 0]));
    colormap(gray)
    axis equal
end

guidata(hObject,handles);


% --- Executes on button press in removedust.
function removedust_Callback(hObject, eventdata, handles)
% hObject    handle to removedust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = getappdata(0, 'data');
handles.currentRCFframe=get(handles.rcfloaded,'Value');

data.Undo.process=data.process(handles.currentRCFframe); %process flag before removedust
data.Undo.handles=handles.process;
data.Undo.data=data.data.ImageArray_cut;

statusmsg(handles,[char(data.parameters.rcffiles(handles.currentRCFframe)), ' starting dust removal...'],'red');

% 05.09.2013 CB:    added roifilt2 afterwards with an average filter
%                   roifill only is to "sharp"
if data.process(handles.currentRCFframe)>=3
    data.data.ImageArray_cut(:,:,1,handles.currentRCFframe)=roifill(data.data.ImageArray_cut(:,:,1,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray_cut(:,:,2,handles.currentRCFframe)=roifill(data.data.ImageArray_cut(:,:,2,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray_cut(:,:,3,handles.currentRCFframe)=roifill(data.data.ImageArray_cut(:,:,3,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray_cut(:,:,4,handles.currentRCFframe)=roifill(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray_cut(:,:,1,handles.currentRCFframe)=roifilt2(fspecial('average',handles.region), data.data.ImageArray_cut(:,:,1,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray_cut(:,:,2,handles.currentRCFframe)=roifilt2(fspecial('average',handles.region), data.data.ImageArray_cut(:,:,2,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray_cut(:,:,3,handles.currentRCFframe)=roifilt2(fspecial('average',handles.region), data.data.ImageArray_cut(:,:,3,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray_cut(:,:,4,handles.currentRCFframe)=roifilt2(fspecial('average',handles.region), data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles.data.bwIR);
    plotrcf(data.data.ImageArray_cut,handles);
else
    data.data.ImageArray(:,:,1,handles.currentRCFframe)=roifill(data.data.ImageArray(:,:,1,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray(:,:,2,handles.currentRCFframe)=roifill(data.data.ImageArray(:,:,2,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray(:,:,3,handles.currentRCFframe)=roifill(data.data.ImageArray(:,:,3,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray(:,:,4,handles.currentRCFframe)=roifill(data.data.ImageArray(:,:,4,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray(:,:,1,handles.currentRCFframe)=roifilt2(fspecial('average',handles.region), data.data.ImageArray(:,:,1,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray(:,:,2,handles.currentRCFframe)=roifilt2(fspecial('average',handles.region), data.data.ImageArray(:,:,2,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray(:,:,3,handles.currentRCFframe)=roifilt2(fspecial('average',handles.region), data.data.ImageArray(:,:,3,handles.currentRCFframe),handles.data.bwIR);
    data.data.ImageArray(:,:,4,handles.currentRCFframe)=roifilt2(fspecial('average',handles.region), data.data.ImageArray(:,:,4,handles.currentRCFframe),handles.data.bwIR);
    plotrcf(data.data.ImageArray,handles);
end

set(handles.undobutton,'Enable','On');
data.process(handles.currentRCFframe)=4;

setappdata(0, 'data', data);
guidata(hObject,handles);

statusmsg(handles,[char(data.parameters.rcffiles(handles.currentRCFframe)), ' dust succesfully removed...'],'blue');


% --- Executes on button press in saveall.
function saveall_Callback(hObject, eventdata, handles)
% hObject    handle to saveall (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getappdata(0,'data');

set(handles.undobutton,'Enable','Off');
savedata=rmfield(data,'Undo');
if ~isempty(data.parameters.rcfpath)
    savedata.parameters.rcfpath=uigetdir(data.parameters.rcfpath);
else
    savedata.parameters.rcfpath=uigetdir;
end
br=0;

if savedata.parameters.rcfpath == 0
    statusmsg(handles,'save all aborted...nothing done !','red');
else
    %save all data to disk in folder rcfpath
    for frame = 1:length(savedata.parameters.rcffiles)
      file_name = fullfile(savedata.parameters.rcfpath, savedata.parameters.rcffiles{frame});
      oldinfo = imfinfo(fullfile(data.parameters.rcfpath, data.parameters.rcffiles{frame}));
      xres = oldinfo.XResolution;
      yres = oldinfo.YResolution;
      isthere=exist(file_name,'file');
      user='Yes';
      
      if isthere==2
          user=questdlg(['file ', char(savedata.parameters.rcffiles{frame}), ' already exists, overwrite ?']);
      end
      
      if strcmp(user,'Yes')
          statusmsgfast(handles,['file ', char(savedata.parameters.rcffiles{frame}), ' saved...'],'blue');
                            
          if savedata.process(frame)>=3
              if size(data.data.fileinfo,1) ~= 1
                  % more channels if IR part is present
                  savetiff4D(file_name,savedata.data.ImageArray_cut,frame,xres,yres);
              else
                  % old scanner, remove 4th channel ... CB
                  savetiff4D(file_name,savedata.data.ImageArray_cut(:,:,1:3),frame,xres,yres);
              end
          else
              if size(data.data.fileinfo,1) ~= 1
                  % more channels if IR part is present
                  savetiff4D(file_name,savedata.data.ImageArray,frame,xres,yres);
              else
                  % old scanner, remove 4th channel ... CB
                  savetiff4D(file_name,savedata.data.ImageArray(:,:,1:3),frame,xres,yres);
              end
          end
              
      elseif strcmp(user,'No')
          statusmsg(handles,['file ', char(savedata.parameters.rcffiles{frame}), ' not saved !'],'red');
      else
          br=1;
          break
      end
      
    end
    
    if br==0;
        statusmsg(handles,'save all finished...all done !','blue');
    else
        statusmsg(handles,'save all aborted during process !','red');
    end    
end


% --- Executes on button press in close.
function close_Callback(hObject, eventdata, handles)
% hObject    handle to close (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close


function statustxt_Callback(hObject, eventdata, handles)
% hObject    handle to statustxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of statustxt as text
%        str2double(get(hObject,'String')) returns contents of statustxt as a double


% --- Executes during object creation, after setting all properties.
function statustxt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to statustxt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in gridbutton.
function gridbutton_Callback(hObject, eventdata, handles)
% hObject    handle to gridbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of gridbutton
handles.grid=get(hObject,'Value');

if handles.grid==1
    grid minor
else
    grid off
end

guidata(hObject,handles);


function rotate_Callback(hObject, eventdata, handles)
% hObject    handle to rotate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rotate as text
%        str2double(get(hObject,'String')) returns contents of rotate as a double
data=getappdata(0,'data');
handles.currentRCFframe=get(handles.rcfloaded,'Value');

handles.rotangle=str2double(get(hObject,'String'));
if ~isnan(handles.rotangle)
    plotrcfdata(imrotate(data.data.ImageArray(:,:,1:3,handles.currentRCFframe),handles.rotangle,'bicubic','crop'),handles);
    guidata(hObject,handles);
else
    handles.rotangle=0;
    set(handles.rotate,'String','0');
    guidata(hObject,handles);
    statusmsg(handles, 'error: rotation angle must be a number !!!','red');
end


% --- Executes during object creation, after setting all properties.
function rotate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rotate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in rotateit.
function rotateit_Callback(hObject, eventdata, handles)
% hObject    handle to rotateit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = getappdata(0, 'data');
handles.currentRCFframe=get(handles.rcfloaded,'Value');

data.Undo.process=data.process(handles.currentRCFframe); %process flag before rotation
if data.Undo.process==3
    data.Undo.data=data.data.ImageArray_cut;
else
    data.Undo.data=data.data.ImageArray;
end

data.data.ImageArray(:,:,:,handles.currentRCFframe)=imrotate(data.data.ImageArray(:,:,:,handles.currentRCFframe),handles.rotangle,'bicubic','crop');

plotrcf(data.data.ImageArray,handles);

data.process(handles.currentRCFframe)=2;

setappdata(0,'data',data);

handles.rotangle=0;
set(handles.rotate,'String','0');
set(handles.undobutton,'Enable','On');
set(handles.rotate,'Enable','Off');
set(handles.rotateit,'Enable','Off');
set(handles.corners_button,'Enable','Off');

guidata(hObject,handles);

statusmsg(handles,[char(data.parameters.rcffiles(handles.currentRCFframe)), ' succesfully rotated...'],'blue');


function regionedit_Callback(hObject, eventdata, handles)
% hObject    handle to regionedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of regionedit as text
%        str2double(get(hObject,'String')) returns contents of regionedit as a double
data=getappdata(0,'data');
handles.currentRCFframe=get(handles.rcfloaded,'Value');
set(handles.undobutton,'Enable','Off');

if ~isnan(str2double(get(hObject,'String')));
    handles.region=round(str2double(get(hObject,'String')));
    if 10<handles.region
        handles.region=10;
        set(handles.regionedit,'String',num2str(handles.region));
        if data.process(handles.currentRCFframe)>=3
            handles.data.bwIR=findscratches(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles);
            plotrcfdata(imoverlay(data.data.ImageArray_cut(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
        else
            handles.data.bwIR=findscratches(data.data.ImageArray(:,:,4,handles.currentRCFframe),handles);
            plotrcfdata(imoverlay(data.data.ImageArray(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
        end
        guidata(hObject,handles);
        statusmsg(handles, 'error: pixel radius must be smaller/equal 10 pixels !!!','red');    

    elseif handles.region>0
        set(handles.regionedit,'String',num2str(handles.region));
        if data.process(handles.currentRCFframe)>=3
            handles.data.bwIR=findscratches(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles);
            plotrcfdata(imoverlay(data.data.ImageArray_cut(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
        else
            handles.data.bwIR=findscratches(data.data.ImageArray(:,:,4,handles.currentRCFframe),handles);
            plotrcfdata(imoverlay(data.data.ImageArray(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
        end
        guidata(hObject,handles);
    else
        handles.region=1;
        set(handles.regionedit,'String',num2str(handles.region));
        if data.process(handles.currentRCFframe)>=3
            handles.data.bwIR=findscratches(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles);
            plotrcfdata(imoverlay(data.data.ImageArray_cut(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
        else
            handles.data.bwIR=findscratches(data.data.ImageArray(:,:,4,handles.currentRCFframe),handles);
            plotrcfdata(imoverlay(data.data.ImageArray(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
        end
        guidata(hObject,handles);
        statusmsg(handles, 'error: pixel radius must be at least 1 pixel !!!','red');
    end
else
    set(handles.regionedit,'String',num2str(handles.region));
    guidata(hObject,handles);
    statusmsg(handles, 'error: pixel radius must be a number !!!','red');
end


% --- Executes during object creation, after setting all properties.
function regionedit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to regionedit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in undobutton.
function undobutton_Callback(hObject, eventdata, handles)
% hObject    handle to undobutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data=getappdata(0,'data');
handles.currentRCFframe=get(handles.rcfloaded,'Value');

if data.Undo.process==3 %process number before undo
    data.data.ImageArray_cut=data.Undo.data;
    data.process(handles.currentRCFframe)=3;
elseif data.Undo.process==2
    data.data.ImageArray=data.Undo.data;
    data.process(handles.currentRCFframe)=2;
elseif data.Undo.process==4
    data.data.ImageArray_cut=data.Undo.data;
    data.process(handles.currentRCFframe)=4;
elseif data.Undo.process==1
    data.data.ImageArray=data.Undo.data;
    data.process(handles.currentRCFframe)=1;
elseif data.Undo.process==0
    data.data.ImageArray=data.Undo.data;
    data.process(handles.currentRCFframe)=0;
end

handles.process=data.Undo.handles;
set(handles.undobutton,'Enable','Off');

setappdata(0,'data',data);
eventdata='undo';
guidata(hObject,handles);

loadcurr(hObject,eventdata,handles)


% --- Executes when selected object is changed in scratchpanel.
function scratchpanel_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in scratchpanel 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)
data=getappdata(0,'data');
handles.currentRCFframe=get(handles.rcfloaded,'Value');
current=get(eventdata.NewValue,'Tag');
set(handles.undobutton,'Enable','Off');

if strcmp(current,'darkbutton')
    handles.panel = 0;
    if data.process(handles.currentRCFframe)>=3
        handles.data.bwIR=findscratches(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles);
        plotrcfdata(imoverlay(data.data.ImageArray_cut(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
    else
        handles.data.bwIR=findscratches(data.data.ImageArray(:,:,4,handles.currentRCFframe),handles);
        plotrcfdata(imoverlay(data.data.ImageArray(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
    end
elseif strcmp(current, 'brightbutton')
    handles.panel = 1;
    if data.process(handles.currentRCFframe)>=3
        handles.data.bwIR=findscratches(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles);
        plotrcfdata(imoverlay(data.data.ImageArray_cut(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
    else
        handles.data.bwIR=findscratches(data.data.ImageArray(:,:,4,handles.currentRCFframe),handles);
        plotrcfdata(imoverlay(data.data.ImageArray(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
    end
else
    handles.panel = 2;
    if data.process(handles.currentRCFframe)>=3
        handles.data.bwIR=findscratches(data.data.ImageArray_cut(:,:,4,handles.currentRCFframe),handles);
        plotrcfdata(imoverlay(data.data.ImageArray_cut(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
    else
        handles.data.bwIR=findscratches(data.data.ImageArray(:,:,4,handles.currentRCFframe),handles);
        plotrcfdata(imoverlay(data.data.ImageArray(:,:,1:3,handles.currentRCFframe), handles.data.bwIR, [1 0 0]),handles);
    end
end

guidata(hObject,handles);


% --- Executes on button press in fixx_box.
function fixx_box_Callback(hObject, eventdata, handles)
% hObject    handle to fixx_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fixx_box
try
    if ishandle(handles.rectangle)
        maskit_Callback(hObject, eventdata, handles);
    end
end


% --- Executes on button press in fixy_box.
function fixy_box_Callback(hObject, eventdata, handles)
% hObject    handle to fixy_box (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of fixy_box
try
    if ishandle(handles.rectangle)
        maskit_Callback(hObject, eventdata, handles);
    end
end


function rotate_KeyPressFcn(hObject, eventdata, handles)
% --- Executes on key press with focus on rotate and none of its controls.
% rotate by steps� if up or down arrow keys are pressed while in rotate
% field
handles.rotangle = str2double(get(hObject,'String'));
handles.currentRCFframe=get(handles.rcfloaded,'Value');

if ~isnan(handles.rotangle)
    steps = .5;
    data = getappdata(0,'data');
    if strcmp(eventdata.Key,'uparrow')
        handles.rotangle = handles.rotangle + steps;
    elseif strcmp(eventdata.Key,'downarrow')
        handles.rotangle = handles.rotangle - steps;
    end
    guidata(hObject,handles);
    plotrcfdata(imrotate(data.data.ImageArray(:,:,1:3,handles.currentRCFframe), handles.rotangle,'bicubic','crop'),handles);
else
    handles.rotangle = 0;
    statusmsg(handles, 'error: rotation angle must be a number !!!','red');
end
set(hObject,'String', handles.rotangle);
guidata(hObject,handles);


function corners_button_Callback(hObject, eventdata, handles)
% --- Executes on button press in corners_button.
[x, y] = ginput(4);
hold on
plot([x; x(1)], [y; y(1)])
hold off

a1 = atan2(y(2)-y(1), x(2)-x(1))*180/pi;
a1 = rem(a1,90);
if (a1/45 >= 1)
    a1 = -90+a1;
elseif (a1/45 <= -1)
    a1 = 90+a1;
end

a2 = atan2(y(3)-y(2), x(3)-x(2))*180/pi;
a2 = rem(a2,90);
if (a2/45 >= 1)
    a2 = -90+a2;
elseif (a2/45 <= -1)
    a2 = 90+a2;
end

a3 = atan2(y(4)-y(3), x(4)-x(3))*180/pi;
a3 = rem(a3,90);
if (a3/45 >= 1)
    a3 = -90+a3;
elseif (a3/45 <= -1)
    a3 = 90+a3;
end

a4 = atan2(y(1)-y(4), x(1)-x(4))*180/pi;
a4 = rem(a4,90);
if (a4/45 >= 1)
    a4 = -90+a4;
elseif (a4/45 <= -1)
    a4 = 90+a4;
end

handles.rotangle = (a1 + a2 +a3 +a4)/4;
set(handles.rotate,'String', handles.rotangle);

handles.xcenter = round(sum(x)/4);
handles.ycenter = round(sum(y)/4);

set(handles.xcentermask,'String', num2str(handles.xcenter));
set(handles.ycentermask,'String', num2str(handles.ycenter));

guidata(hObject,handles);

try
    if ishandle(handles.rectangle)
        maskit_Callback(hObject, eventdata, handles);
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%helper functions:
function loadcurr(hObject, eventdata, handles)
data = getappdata(0, 'data');
handles.currentRCFframe=get(handles.rcfloaded,'Value');

if data.process(handles.currentRCFframe)>=3 
    plotrcf(data.data.ImageArray_cut,handles);
    data.Undo.data = data.data.ImageArray_cut;
    data.Undo.process = data.process(handles.currentRCFframe);
    data.Undo.handles = handles.process;
    [handles.height, handles.width, ~, ~]=size(data.data.ImageArray_cut);
    
    set(handles.rotateit,'Enable','Off');
    set(handles.rotate,'Enable','Off');
    set(handles.corners_button,'Enable','Off');
    set(handles.xcentermask,'Enable','Off');
    set(handles.ycentermask,'Enable','Off');
    set(handles.maskit,'Enable','Off');
    set(handles.cutit,'Enable','Off');
    set(handles.fixx_box,'Value',0);
    set(handles.fixy_box,'Value',0);
    set(handles.fixx_box,'Enable','Off');
    set(handles.fixy_box,'Enable','Off');
    
    set(handles.irthreslider,'Enable','On');
    set(handles.irthresval,'Enable','On'); 
    set(handles.showir,'Enable','On');
    set(handles.removedust,'Enable','On');
    set(handles.regionedit,'Enable','On');
    set(handles.brightbutton,'Enable','On');
    set(handles.darkbutton,'Enable','On');
    set(handles.bothbutton,'Enable','On');
    set(handles.corners_button,'Enable','Off');
    
else
    plotrcf(data.data.ImageArray,handles);
    data.Undo.data = data.data.ImageArray;
    data.Undo.process = data.process(handles.currentRCFframe);
    data.Undo.handles = handles.process;
    [handles.height, handles.width, ~, ~]=size(data.data.ImageArray);
    
    if handles.process~=3
       set(handles.xmask,'Enable','On');
       set(handles.ymask,'Enable','On');
       set(handles.fixx_box,'Value',0);
       set(handles.fixy_box,'Value',0);
       set(handles.fixx_box,'Enable','On');
       set(handles.fixy_box,'Enable','On');
    end
    
    if data.process(handles.currentRCFframe)==2
        set(handles.rotateit,'Enable','Off');
        set(handles.rotate,'Enable','Off');
    else
        set(handles.rotateit,'Enable','On');
        set(handles.rotate,'Enable','On');
    end
    
    set(handles.xcentermask,'Enable','On');
    set(handles.ycentermask,'Enable','On');
    set(handles.maskit,'Enable','On');
    set(handles.cutit,'Enable','On');
    set(handles.fixx_box,'Value',0);
    set(handles.fixy_box,'Value',0);
    set(handles.fixx_box,'Enable','On');
    set(handles.fixy_box,'Enable','On');
    
    set(handles.irthreslider,'Enable','Off');
    set(handles.irthresval,'Enable','Off');
    set(handles.showir,'Enable','Off');
    set(handles.removedust,'Enable','Off');
    set(handles.regionedit,'Enable','Off');
    set(handles.brightbutton,'Enable','Off');
    set(handles.darkbutton,'Enable','Off');
    set(handles.bothbutton,'Enable','Off');
    set(handles.corners_button,'Enable','On');
end

set(handles.undobutton,'Enable','Off');
set(handles.gridbutton,'Enable','On');
set(handles.rotate,'String','0');
handles.rotangle = 0;
setappdata(0,'data',data);

guidata(hObject,handles);

if strcmp(eventdata,'undo')
    statusmsg(handles,[char(data.parameters.rcffiles(handles.currentRCFframe)),' undone...'],'red');
else
    statusmsg(handles,[char(data.parameters.rcffiles(handles.currentRCFframe)),' loaded...'],'blue');
end
guidata(hObject,handles);

function statusmsg(handles,txt,color)
set(handles.statustxt,'String',txt,'ForegroundColor',color);
% pause(3.0);
set(handles.statustxt,'String','','ForegroundColor','black');

function statusmsgfast(handles,txt,color)
set(handles.statustxt,'String',txt,'ForegroundColor',color);
% pause(0.5);
set(handles.statustxt,'String','','ForegroundColor','black');

function plotrcf(data,handles)
handles.grid=get(handles.gridbutton,'Value');
handles.currentRCFframe=get(handles.rcfloaded,'Value');
set(gcf,'WindowButtonUpFcn','');
imagesc(data(:,:,1:3,handles.currentRCFframe));
axis equal

if handles.grid==1
    grid minor
else
    grid off
end

set(gca,'Xcolor','red');
set(gca,'Ycolor','red');

set(gca,'LineWidth',1.5);

function plotrcfdata(data,handles)
handles.grid=get(handles.gridbutton,'Value');
set(gcf,'WindowButtonUpFcn','');
imagesc(data);
axis equal

if handles.grid==1
    grid minor
    
else
    grid off
end

set(gca,'Xcolor','red');
set(gca,'Ycolor','red');

set(gca,'LineWidth',1.5);

function update_fields(hObject,~,handles)
positions=get(handles.rectangle,'Position');

handles.dxmask=positions(3);
handles.dymask=positions(4);

handles.xcenter=round(positions(1)+handles.dxmask/2);
handles.ycenter=round(positions(2)+handles.dymask/2);


set(handles.xcentermask,'String',num2str(handles.xcenter));
set(handles.ycentermask,'String',num2str(handles.ycenter));
set(handles.xmask,'String',num2str(handles.dxmask));
set(handles.ymask,'String',num2str(handles.dymask));

maskit_Callback(hObject,0,handles)

%the interactive version for calling in draggable function during move
function update_fieldsint(rectobj)
handles=getappdata(0,'handles_old');
hObject=getappdata(0,'hObject_old');

positions=get(rectobj,'Position');

handles.dxmask=positions(3);
handles.dymask=positions(4);

handles.xcenter=round(positions(1)+handles.dxmask/2);
handles.ycenter=round(positions(2)+handles.dymask/2);

set(handles.xcentermask,'String',num2str(handles.xcenter));
set(handles.ycentermask,'String',num2str(handles.ycenter));
set(handles.xmask,'String',num2str(handles.dxmask));
set(handles.ymask,'String',num2str(handles.dymask));
guidata(hObject,handles);

function savetiff4D(file_name,data,frame,resX,resY)
filepoint=Tiff(file_name,'w');

tag.Photometric=Tiff.Photometric.RGB;
tag.Compression=Tiff.Compression.None;
tag.ResolutionUnit=Tiff.ResolutionUnit.Inch;
tag.BitsPerSample=16;
tag.ImageLength=size(data,1);
tag.ImageWidth=size(data,2);
tag.SamplesPerPixel=3;
tag.XResolution=resX;
tag.YResolution=resY;
tag.SubFileType=Tiff.SubFileType.Default;

tag.RowsPerStrip = 1;
tag.PlanarConfiguration=Tiff.PlanarConfiguration.Chunky;
tag.Software='MATLAB - RCF NIKON postprocessor';

%RGB part of Image
filepoint.setTag(tag);
filepoint.write(data(:,:,1:3,frame));

if size(data,3) ~= 3
    %IR part of Image
    filepoint.writeDirectory()
    tag.SubFileType=Tiff.SubFileType.Mask;
    tag.Photometric=Tiff.Photometric.MinIsBlack;
    tag.SamplesPerPixel=1;
    filepoint.setTag(tag);
    filepoint.write(data(:,:,4,frame));
end

filepoint.close();

function [ outputmatrix ] = findscratches(inputmatrix, handles)
ysize=size(inputmatrix,1); %rows
xsize=size(inputmatrix,2); %colums

outputmatrix=findscratchesc(ysize,xsize,double(inputmatrix),handles.region,handles.sliderval,handles.panel);
outputmatrix=logical(outputmatrix);

%blow matrix by radius rad off => disk-mask
se=strel('disk',handles.region, 4);
outputmatrix=imdilate(outputmatrix,se);
