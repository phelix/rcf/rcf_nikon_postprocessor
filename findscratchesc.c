/*
 * Copyright (C) 2021 by it's contributors.
 * Some rights reserved. See COPYING, CREDITS.
 *
 * This file is part of RCF Nikon postprocessor.
 *
 * RCF Nikon postprocessor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * long with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "mex.h"
#include "matrix.h"

#define max_Olli(a,b) \
   ( a > b ? a : b )

#define min_Olli(a,b) \
   ( a < b ? a : b )

void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[])
{
    /*called from matlab like: 
     a=findscratchesc(ysize,xsize,double(inputmatrix),region,threshold,cond);
     
     where:
     ysize => number of rows inputmatrix = size(inputmatrix,1)
     xsize => number of colums inputmatrix = size(inputmatrix,2)
     region => number of pixels to define mean arround pixel
     threshold => between 0 and 1; deviation from mean condition
     cond => 0=find dark scratches, 1=find bright scratches, 2=find both    
     a=outputmatrix
    */ 
    
    #define inputmatrix(i,j) c[i+j*M]
    #define outputmatrix(i,j) idxptr[i+j*M]
            
    const mxArray *ysizegrid, *xsizegrid, *cgrid, *regiongrid, *thresgrid, *condgrid; 
    mxArray *idx;
    double *idxptr;
    double *xsizepnt, *ysizepnt, *c, *regionpnt, *threspnt, *condpnt;
    int j, i, k, h, lowk, lowh, highk, highh, ysize, xsize, region, cond, M, counts;
    double thres, mean;
    mwSize dims[2];
    
    /*catch pointer ysize*/
    ysizegrid = prhs[0];
    ysizepnt = mxGetPr(ysizegrid);
    ysize=ysizepnt[0];
    
    /*catch pointer xsize*/
    xsizegrid = prhs[1];
    xsizepnt = mxGetPr(xsizegrid);
    xsize=xsizepnt[0];
    
    /*catch pointer inputmatrix*/
    cgrid = prhs[2];
    c = mxGetPr(cgrid);
    M = mxGetM(cgrid);
    
    /*catch pointer region*/
    regiongrid = prhs[3];
    regionpnt = mxGetPr(regiongrid);
    region=regionpnt[0];
    
    /*catch pointer threshold*/
    thresgrid = prhs[4];
    threspnt = mxGetPr(thresgrid);
    thres=threspnt[0];
    
    /*catch pointer condition*/
    condgrid = prhs[5];
    condpnt = mxGetPr(condgrid);
    cond=condpnt[0];
    
       
    /*create output matrix a; size(a)= ysize, xsize*/        
    dims[0] = ysize; dims[1] = xsize;
    plhs[0] = idx = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
    
    if (idx==NULL) 
    {
       
        dims[0] = 0; dims[1] = 0;
        plhs[0] = mxCreateNumericArray(2, dims, mxDOUBLE_CLASS, mxREAL);
        return;
    }
    
    /*assign output to righthand pointer*/
    idxptr = mxGetPr(idx);
    
    /*find scratches in inputmatrix*/
    for(i = 0; i < xsize; i++) {
        for(j = 0; j < ysize; j++) {          
            
            lowk = max_Olli(j-region,0);
            highk = min_Olli(j+region,ysize-1);
            lowh = max_Olli(i-region,0);
            highh = min_Olli(i+region,xsize-1);
            mean = 0;
            counts = 0;
                
            for (k = lowk; k<=highk; k++) {
                for (h = lowh ; h<=highh; h++) {
                    mean = mean + inputmatrix(k,h);
                    counts = counts + 1;
                }
            }
            mean = (mean-inputmatrix(j,i))/(counts-1);
            
            if(cond==0)
            {
                if(inputmatrix(j,i)<thres*1.1*mean) outputmatrix(j,i)=1;
            }
            else if(cond==1)
            {
                if(inputmatrix(j,i)*thres*1.25>mean) outputmatrix(j,i)=1;
            }
            else if(cond==2)
            {
                if((inputmatrix(j,i)<thres*1.1*mean)||(inputmatrix(j,i)*thres*1.25>mean)) outputmatrix(j,i)=1;
            }
            
        }
    }
    
    return;
}
